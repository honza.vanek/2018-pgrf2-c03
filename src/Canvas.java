import imagedata.*;
import io.vavr.Function3;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.Stream;
import objectdata.Cube;
import objectops.RenderSolid;
import objectops.RenderWireframe;
import org.jetbrains.annotations.NotNull;
import rasterops.*;
import transforms.*;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.function.BiFunction;
import java.util.function.Function;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * trida pro kresleni na platno: zobrazeni pixelu
 *
 * @author PGRF FIM UHK
 * @version 2017
 */

/* TODO:
	* X obrazek (pixel) s hloubkou
		(2 samost. obr
		nebo nova trida DepthPixel
		nebo Tuple2<Col, Double>)
	* X obrazek se z-testem
	* X rasterizer trojuhelniku
	* RendererSolid (s podporou topologie TRIANGLE_LIST)
	* teleso z trojuhelniku
	* scena
 */

public class Canvas {

	private final JFrame frame;
	private final JPanel panel;
	private final BufferedImage img;
	private @NotNull Image<Tuple2<Col, Double>> image;
	private final @NotNull Presenter<Tuple2<Col, Double>, Graphics> presenter;
	private final @NotNull LineRendererLerp<Tuple2<Col, Double>> lineRenderer;
	private final @NotNull TriangleRenderer<Tuple2<Col, Double>> triangleRenderer;

	private int sC, sR;

	public Canvas(final int width, final int height) {
		frame = new JFrame();

		frame.setLayout(new BorderLayout());
		frame.setTitle("UHK FIM PGRF : " + this.getClass().getName());
		frame.setResizable(false);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
/*
		image = new ImageUgly<>(
			img,
			//Function<UglyPixelTYpe,Integer> toRGB, kde UglyPixelType = Integer
			Function.identity()
			,
			//Function<Integer, UglyPixelTYpe> toUgly, kde UglyPixelType = Integer
			Function.identity()
		);
		presenter = new PresenterUgly<>();
/*/
		final Image<Tuple2<Col, Double>> emptyImage =
			new ImageVavr<>(width, height, Tuple.of(new Col(0xffff0000), 1.0));
		image = new ImageBlender<>(emptyImage,
			(newValue, oldValue) ->
//				newValue.apply(
//					(newCol, newDepth) -> oldValue.apply(
//						(oldCol, oldDepth) ->
//							newDepth < oldDepth ? newValue : oldValue
//					)
//				)
				newValue._2 < oldValue._2 ? newValue : oldValue
		);
		presenter = new PresenterUniversal<>(
				depthPixel -> depthPixel._1.getARGB()
		);
//*/
		final Function3<Tuple2<Col, Double>, Tuple2<Col, Double>, Double, Tuple2<Col, Double>>
				depthPixelLerp =
					(v1, v2, t) -> Tuple.of(
							v1._1.mul(1 - t).add(v2._1.mul(t)),
							v1._2 * (1 - t) + v2._2 * t
					);



		lineRenderer = new LineRendererDDALerp<>(
				depthPixelLerp
		);

		triangleRenderer = new TriangleRendererScan<>(
				depthPixelLerp
		);


		panel = new JPanel() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				present(g);
			}
		};

		panel.setPreferredSize(new Dimension(width, height));

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				sC = e.getX();
				sR = e.getY();
			}
		});
		panel.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				clear();
				final double sX = 2.0 * sC / (width - 1) - 1;
				final double sY = 1 - 2.0 * sR / (height - 1);
				final double eX = 2.0 * e.getX() / (width - 1) - 1;
				final double eY = 1 - 2.0 * e.getY() / (height - 1);
				image = triangleRenderer.render(image,
						0,0,sX,sY,eX,eY,
						Tuple.of(new Col(0xff0000), 1.0),
						Tuple.of(new Col(0x00ff00), 1.0),
						Tuple.of(new Col(0x0000ff), 1.0)
				);
				panel.repaint();
			}
		});

		frame.add(panel, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
	}

	public void clear() {
		/*
		Graphics gr = img.getGraphics();
		gr.setCol(new Col(0x2f2f2f));
		gr.fillRect(0, 0, img.getWidth(), img.getHeight());
		*/
		image = image.cleared(Tuple.of(new Col(0xff2f2f2f), 1.0));
	}

	public void present(final Graphics graphics) {
//		graphics.drawImage(img, 0, 0, null);
		presenter.present(image, graphics);
	}

	public void draw() {
		clear();
//		image = Stream.range(1, 10).foldLeft(image,
//			(currentImage, i) -> currentImage.withValue(10,10 + i, new Col(0xff00ff00))
//		);
		/*
		image = lineRenderer.render(
				image, -0.9, -0.8, 0.9, 0.8, new Col(0xffff00ff), new Col(0xffffff00));
		image = lineRenderer.render(
				image, -0.9, 0.8, 0.9, -0.8, new Col(0xffff00ff), new Col(0xffffff00));
		image = lineRenderer.render(
				image, -0.9, -0.2, 0.9, -0.2, new Col(0xffff00ff), new Col(0xffffff00));
		image = new FloodFill4<Col>().fill(image,
				image.getWidth() / 2, image.getHeight() / 2 +5,
				new Col(0xffffff00),
				value -> value.getARGB() == 0xff2f2f2f
		);
		//*/

		final Cube cube = new Cube();
		image =
				new RenderSolid<Tuple2<Col, Double>, Point3D>(
						Function.identity(),
						(vertex, z) -> Tuple.of(new Col(vertex), z),
						(v1, v2, t) -> v1.mul(1 - t).add(v2.mul(t)),
						lineRenderer, triangleRenderer)
						.render(image, cube, cube.getParts().get(1),
								new Camera()
										.withPosition(new Vec3D(1.1,1.1,1.1))
										.withAzimuth(Math.PI + Math.PI / 4)
										.withZenith(-Math.atan(2.0 / 5.0))
										.getViewMatrix()
										.mul(new Mat4PerspRH(
												Math.PI / 3,
												image.getHeight() / (double) image.getWidth(),
												0.3, 1000
										)),
								Tuple.of(new Col(1.0f, 1, 0), 1.0));

	}

	public void start() {
		draw();
		panel.repaint();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Canvas(800, 600)::start);
	}

}